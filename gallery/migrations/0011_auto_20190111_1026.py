# Generated by Django 2.1.2 on 2019-01-11 10:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0010_auto_20190108_1010'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photograph',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=244)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.AlterField(
            model_name='album',
            name='publication',
            field=models.CharField(blank=True, choices=[('ALL', 'All'), ('WK', 'Week-End'), ('LM', 'Le Mauricien'), ('SCP', 'Scope'), ('TRF', 'Turf'), ('WEB', 'Web'), ('NN', 'Autre')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='album',
            name='photograph',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='album', to='gallery.Photograph'),
        ),
    ]
