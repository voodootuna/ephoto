import datetime
import calendar





def publication_dates(publication='LM'):
    #print(publication)
    skip_day = [6] #skip_day type list of days to skip in week  (0-based weekday number)
    extra_days = 3
    if publication == 'WK':
        skip_day = range(0,6)
    elif publication == 'SCP':
        extra_days = 8
        skip_day = [i for i in range(0,7) if i != 2 ]
    elif publication == 'TRF':
        skip_day = [i for i in range(0,7) if i != 3 ]
        extra_days = 8

    today = datetime.datetime.today().date()
    #print('today : ',today.day)

    days_left_end_month = calendar.monthrange(today.year, today.month)[1] - today.day + 1  + extra_days
  
    dates = ['{}'.format(today + datetime.timedelta(days=i)) for i in range(days_left_end_month ) if (today + datetime.timedelta(days=i)).weekday() not in skip_day]

    return dates

"""
for i in publication_dates(publication='LM'):
    print(i)
print('----------------- ')
print('Scope: ')
"""





