import uuid
from django.db import models
from django.contrib import admin
from django.core.files.storage import FileSystemStorage

from django.template.defaultfilters import slugify


PUBLICATIONS = (
	('ALL', 'All'),
	('LM', 'Le Mauricien'),
	('WK', 'Week-End'),
	('SCP', 'Scope'),
	('TRF', 'Turf'),
	('WEB', 'Web'),
	('NN', 'Autre'),
	)


class MontagePublicationFolder(models.Model):
	publication = models.CharField(choices=PUBLICATIONS, max_length=20, blank=True, null=True)
	created = models.DateTimeField(auto_now_add=True)
	publish_date =  models.DateField(null=True, blank=True)


	def __str__(self):
		return '{} du {}'.format(self.publication, self.publish_date)


	def cover(self):
		if self.montage_subfolder:
			return '{}'.format(self.montage_subfolder.all()[0].images.all()[0].file.url)
		return None


class MontageSubFolder(models.Model):
	publication_folder = models.ForeignKey('gallery.MontagePublicationFolder', null=True, blank=True, related_name='montage_subfolder', editable=True, on_delete=models.SET_NULL)
	title = models.CharField(max_length=244, blank=True, null=True)
	images = models.ManyToManyField('gallery.Image', blank=True, related_name="montage_images")

	def __str__(self):
		return '{}'.format(self.title)


	def cover(self):
		if self.images:
			return '{}'.format(self.images.all()[0].file.url)
		return None



class Image(models.Model):
	album = models.ForeignKey('gallery.Album', null=True, blank=True, related_name="images", editable=True, on_delete=models.SET_NULL)
	title = models.CharField(max_length=244, blank=True, null=True)
	reference = models.CharField(max_length=244, blank=True, null=True)
	created =  models.DateTimeField(auto_now_add=True, null=True,)
	description =  models.CharField(max_length=244, blank=True, null=True)
	file = models.ImageField(upload_to='image/%Y/%m/%d', blank = True)


	
	def __unicode__(self):
		return self.title

	def __str__(self):
		return '{}'.format(self.title)


class Album(models.Model):

	is_visible = models.BooleanField(default=True)
	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now_add=True)
	slug = models.SlugField(max_length=50, unique=True, null=True, blank=True)
	tags = models.CharField(max_length=244, blank=True, null=True)
	title = models.CharField(max_length=244, blank=True, null=True)
	location = models.CharField(max_length=244, blank=True, null=True)
	author = models.CharField(max_length=244, blank=True, null=True)
	publication = models.CharField(choices=PUBLICATIONS, max_length=20, blank=True, null=True)
	publish_date =  models.DateTimeField(null=True, blank=True)
	photograph = models.ForeignKey('gallery.Photograph', related_name='album', null=True, blank=True, on_delete=models.SET_NULL)


	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
		super(Album, self).save(*args, **kwargs)



	def __unicode__(self):
		return self.title

	def __str__(self):
		return '{0} ({1})'.format(self.title, self.publish_date)


	def photo_count(self):
		return self.images.count()


	def cover(self):
		cover = {'path':'https://cdn3.iconfinder.com/data/icons/abstract-1/512/no_image-512.png'}
		try:
			cover = self.images.first().file
		except:
			pass

		return cover



class Photograph(models.Model):
	name = models.CharField(max_length=244)
	is_active = models.BooleanField(default=True)

	def __str__(self):
		return '{0}'.format(self.name)

