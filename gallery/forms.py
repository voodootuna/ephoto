from django import forms
from .models import Image, Album, Photograph
from django.forms import inlineformset_factory
from django.forms.models import BaseInlineFormSet
from gallery.models import PUBLICATIONS

class AlbumSearchForm(forms.Form):
    publication = forms.ChoiceField(required=False,  choices=PUBLICATIONS, initial='', widget=forms.Select(attrs={'class': 'form-control'}))
    current_date = forms.DateField(required=False, widget=forms.TextInput(attrs={'class': 'form-control datepicker'}))
    def __init__(self, *args, **kwargs):
        super(AlbumSearchForm, self).__init__(*args, **kwargs)



class AlbumSearchAdvancedForm(forms.Form):
    title = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    publication = forms.ChoiceField(required=False,  choices=PUBLICATIONS, initial='', widget=forms.Select(attrs={'class': 'form-control'}))
    start_date = forms.DateField(required=False, widget=forms.TextInput(attrs={'class': 'form-control datepicker'}))
    end_date = forms.DateField(required=False, widget=forms.TextInput(attrs={'class': 'form-control datepicker2'}))
    photograph = forms.ModelChoiceField(required=False, queryset=Photograph.objects.filter(is_active=True), label="photographer", widget=forms.Select(attrs={'class': 'form-control'}))
    
  


    def __init__(self, *args, **kwargs):
        super(AlbumSearchAdvancedForm, self).__init__(*args, **kwargs)




class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title','file', 'reference']


    #specifying required attribute on widget for validation on formset
    #title = forms.FileField(required=False, widget=forms.TextInput(attrs={'required': False}))
    file = forms.FileField(required=True, widget=forms.FileInput(attrs={'required': True}))
    reference = forms.CharField(required=True, widget=forms.TextInput(attrs={'required': True, }))





    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)

        for field_name, field in self.fields.items(): 
            if field_name == 'file':
                field.widget.attrs['class'] = 'form-control-file'
            else:
                field.widget.attrs['class'] = 'form-control'



class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ['title', 'publication', 'publish_date', 'photograph', 'location']


    title = forms.CharField(required=True)
    publication= forms.ChoiceField(required=True, choices=PUBLICATIONS)
    publish_date= forms.DateField(required=True)
    
    def __init__(self, *args, **kwargs):
        super(AlbumForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name == 'publish_date':
                field.widget.attrs['class'] = 'form-control datepicker'
            else:
                field.widget.attrs['class'] = 'form-control'






ImageFormSet = inlineformset_factory(Album, Image, fields=('album',  'reference', 'title', 'file',), form=ImageForm, extra=1, can_delete=False,)

