import datetime
import json
from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers

from .forms import ImageForm, AlbumForm, ImageFormSet, AlbumSearchForm, AlbumSearchAdvancedForm
from .models import Image, Album, MontagePublicationFolder, MontageSubFolder
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.db import IntegrityError, transaction
from django.contrib import messages
from django.urls import reverse

from accounts.mixins import CustomLoginRequiredMixin
from .utils import publication_dates
from .models import PUBLICATIONS




def index(request):
	return render(request, 'upload_form.html', {})



class ImageListView(ListView):
	model = Image
	template_name = 'image_search.html'  # Default: <app_label>/<model_name>_list.html
	context_object_name = 'photos'  # Default: object_list
	paginate_by = 10
	queryset = Image.objects.all()  # Default: Model.objects.all()


class ImageDetailView(DetailView):
	model = Image
	template_name = 'image_detail_view.html'
	context_object_name = 'image'



class AlbumCreateView(CustomLoginRequiredMixin, CreateView):
	model = Album
	template_name = 'upload_form.html'
	form_class = AlbumForm


	def get(self, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		image_form = ImageFormSet()

		return self.render_to_response(
			self.get_context_data(form=form, image_form=image_form)
			)

	def get_success_url(self):
		return reverse('album-detail', kwargs={'pk' : self.object.pk})

	
	def form_valid(self, form):
		ctx=self.get_context_data()
		images = ctx['image_form']

   
		with transaction.atomic():
			self.object = form.save()

			for image in images.forms:
				print('IMAGES', images)
				print('******')
			if images.is_valid():
				images.instance= self.object
				images.save()


		"""  with transaction.atomic():

				self.object = form.save()
				if images.is_valid():
					images.instance = self.object
					images.save()
		"""

		return super(AlbumCreateView, self).form_valid(form)




	def get_context_data(self, *args, **kwargs):
		ctx = super(AlbumCreateView, self).get_context_data(*args, **kwargs)
		if self.request.POST:
			ctx['form'] = AlbumForm(self.request.POST)
			ctx['image_form'] = ImageFormSet(data=self.request.POST, files=self.request.FILES)
		else:
			ctx['form'] = AlbumForm()
			ctx['image_form'] = ImageFormSet()

		return ctx
	   

  

class AlbumListAdvancedView(CustomLoginRequiredMixin, ListView):
	model = Album
	template_name = 'image_search_advanced.html'  
	context_object_name = 'album'
	form_class = AlbumSearchAdvancedForm
	paginate_by = 8
	allow_empty_first_page=True
	queryset = Album.objects.filter(is_visible=True).order_by('publish_date')

	def get_context_data(self, *args, **kwargs):
		ctx = super(AlbumListAdvancedView, self).get_context_data(*args, **kwargs)
		
		ctx['form'] = AlbumSearchAdvancedForm(self.request.GET)

		return ctx

	def get_queryset(self):
		form = self.form_class(self.request.GET)

		if form.is_valid():

			title = form.cleaned_data['title']
			publication = form.cleaned_data['publication']
			photograph = form.cleaned_data['photograph']
			start_date = form.cleaned_data['start_date']
			end_date = form.cleaned_data['end_date']
			filters = {'is_visible':True}


			if title:
				filters['title__icontains'] = title

			if photograph:
				filters['photograph__name'] = photograph

			if start_date:
				filters['publish_date__gte'] = start_date
	   

			if end_date:
				filters['publish_date__lte'] = end_date


			if (publication) and (publication != 'ALL'):
				filters['publication'] = publication

			print('FILTERS',filters)

			object_list = self.model.objects.filter(**filters).order_by('created')
			return object_list    
		
			
	   
		return Album.objects.filter(is_visible=True).order_by('publish_date')


class AlbumListView(CustomLoginRequiredMixin, ListView):
	model = Album
	template_name = 'image_search.html'  
	context_object_name = 'album'
	form_class = AlbumSearchForm
	paginate_by = 8
	allow_empty_first_page=True
	queryset = Album.objects.filter(is_visible=True).order_by('publish_date')

	def get_context_data(self, *args, **kwargs):
		ctx = super(AlbumListView, self).get_context_data(*args, **kwargs)
		
		ctx['form'] = AlbumSearchForm(self.request.GET)

		return ctx

	def get_queryset(self):
		form = self.form_class(self.request.GET)

		if form.is_valid():

			publication = form.cleaned_data['publication']
			current_date = form.cleaned_data['current_date']
			filters = {'is_visible':True}

			if current_date:
				filters['publish_date__date'] = current_date
			else:
				filters['publish_date__date'] = datetime.datetime.today()
				
				
	   
			if (publication) and (publication != 'ALL'):
				filters['publication'] = publication

			print('FILTERS',filters)


			object_list = self.model.objects.filter(**filters).order_by('created')
			return object_list    
		
		
		
	   
		return Album.objects.filter(is_visible=True).order_by('publish_date')


class AlbumDetailView(CustomLoginRequiredMixin, DetailView):
	model = Album
	context_object_name = 'album'
	template_name = 'album_detail_view.html'

	def get_context_data(self, *args, **kwargs):
		ctx = super(AlbumDetailView, self).get_context_data(*args, **kwargs)
		pub_dates = {'LM':publication_dates(), 
							'WK':publication_dates(publication='WK'), 
							'SCP':publication_dates(publication='SCP'), 
							'TRF':publication_dates(publication='TRF') }
		ctx['pub_dates'] = pub_dates
		ctx['publications']= PUBLICATIONS
		ctx['images'] = self.request.GET.get('images', None)

		return ctx



class AlbumUpdateView(CustomLoginRequiredMixin, UpdateView):
	model = Album
	context_object_name = 'album'
	template_name = 'album_update_view.html'
	form_class = AlbumForm

	def get_success_url(self):
		return reverse('album-detail', kwargs={'pk' : self.object.pk})




class MontageListView(CustomLoginRequiredMixin, ListView):
	model = MontagePublicationFolder
	context_object_name = 'publications'
	template_name = 'montage_list_view.html'
	from_date = datetime.datetime.now() - datetime.timedelta(days=7)
	queryset = MontagePublicationFolder.objects.filter(publish_date__range=[from_date, datetime.datetime.now()])


	def get_queryset(self):

		publication = self.request.GET.get("p", None)
		return MontagePublicationFolder.objects.filter(publication=publication, publish_date__range=[self.from_date, datetime.datetime.now()])



class MontageDetailView(CustomLoginRequiredMixin, ListView):
	model = MontageSubFolder
	context_object_name = 'subfolders'
	template_name = 'montage_sub_list_view.html'


	def get_queryset(self):

		publication_folder_id = self.kwargs.get('pk')
		print('publication_folder_id', publication_folder_id)
		return MontageSubFolder.objects.filter(publication_folder=publication_folder_id)


class SubFolderDetailView(CustomLoginRequiredMixin, DetailView):
	model = MontageSubFolder
	context_object_name = 'subfolder'
	template_name = 'subfolder_detail_view.html'


	




class MontageApiView(CustomLoginRequiredMixin, DetailView):

	def get(self, *args, **kwargs):
		publish_date = self.kwargs.get('date')
		publication = self.kwargs.get('pub')
		print(publish_date, 'date')
		montage_pub = MontagePublicationFolder
		data = {}
		try:
			montage_pub_obj = montage_pub.objects.get(publish_date=publish_date, publication=publication)
			data = serializers.serialize('json', montage_pub_obj.montage_subfolder.all(), fields=('title','pk'))
		
		except montage_pub.DoesNotExist:
			print('does not exist')
			return JsonResponse({ 'status':False,'message':'main folder not found', 'data':None}, status=400)

		return HttpResponse(data, content_type="application/json")



class SendToMontageView(CustomLoginRequiredMixin, TemplateView):
	template_name = 'send_to_montage.html'

	"""
	def get(self, request, *args, **kwargs):
		pass
	"""
	def post(self, *args, **kwargs):
		#print(self.request.POST)

		publication = self.request.POST.get('publication', None)
		pub_date = self.request.POST.get('pub_date', None)
		images = self.request.POST.get('images', None)
		dossier_id = self.request.POST.get('dossier', None)
		new_dossier = self.request.POST.get('new_dossier', None)


		print('publication : ', publication)
		print('pub-date : ', pub_date)
		print('images : ', images)
		print('dossier_id : ', dossier_id)
		print('new_dossier : ', new_dossier)


		with transaction.atomic():
			montage_pub_obj, created = MontagePublicationFolder.objects.get_or_create(publication=publication, publish_date=pub_date)
			
			subfolder_filters = {'publication_folder':montage_pub_obj,
						'title':new_dossier
						}

			if dossier_id == 'new':
				pass
			else:
				subfolder_filters['id']=dossier_id


		
			montage_subfolder_obj, created = MontageSubFolder.objects.get_or_create(**subfolder_filters)

			for i in images.split(','):
				montage_subfolder_obj.images.add(int(i))

			montage_subfolder_obj.save()

			



			print(montage_pub_obj)


		
		return self.render_to_response(self.get_context_data(**kwargs))




