from django.contrib import admin
# Register your models here.
from django.conf.urls import url, include
from django.urls import path
from .views import ImageListView, AlbumListView, AlbumListAdvancedView, AlbumDetailView, AlbumCreateView, ImageDetailView, AlbumUpdateView, MontageListView, SendToMontageView, MontageApiView, MontageListView, MontageDetailView, SubFolderDetailView
from . import views

admin.site.site_title = 'ePhoto Admin'
admin.site.site_header = 'ePhoto Admin'

urlpatterns = [
    path('',  AlbumListView.as_view(), name='albums_search'),
    path('upload/',  AlbumCreateView.as_view(), name='albums_upload'),
    path(r'search/', AlbumListView.as_view(), name='albums_search'),
    path(r'advanced_search/', AlbumListAdvancedView.as_view(), name='albums_advanced_search'),
    path('album/<int:pk>/', AlbumDetailView.as_view(), name='album-detail'),
    path('image/<int:pk>/', ImageDetailView.as_view(), name='image-detail'),
    path('album_edit/<int:pk>/', AlbumUpdateView.as_view(), name='album-update'),
    path('montage/', MontageListView.as_view(), name='montage-list'),
    path('montage_detail/<int:pk>/', MontageDetailView.as_view(), name='montage-detail-view'),
     path('subfolder/<int:pk>/', SubFolderDetailView.as_view(), name='subfolder-detail-view'),
    path('send_to_montage/', SendToMontageView.as_view(), name='montage-send'),
    path('montage_api/<str:date>/<str:pub>', MontageApiView.as_view(), name='montage-api'),


    path('accounts/', include('django.contrib.auth.urls')),



]