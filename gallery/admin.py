from django.contrib import admin
from .models import Image, Album, Photograph, MontagePublicationFolder, MontageSubFolder



class ImageInline(admin.TabularInline):
	model = Image

class AlbumInline(admin.TabularInline):
	model = Album

class SubFolderInline(admin.TabularInline):
	model = MontageSubFolder
	raw_id_fields = ['images']

class AlbumAdmin(admin.ModelAdmin):
 	inlines = [ImageInline,]
 	list_display = ['title', 'publish_date', 'publication','location','modified', 'photo_count']


class ImageAdmin(admin.ModelAdmin):
	list_display = ['title', 'album', 'reference', 'created']
	raw_id_fields = ['album']
	search_fields = ['title', 'album__title', 'reference']


class MontageSubFolderAdmin(admin.ModelAdmin):
	raw_id_fields = ['images']

class MontagePublicationFolderAdmin(admin.ModelAdmin):
		inlines = [SubFolderInline,]
		list_display = ['publish_date', 'publication',]


class PhotographAdmin(admin.ModelAdmin):
	list_display = ['name', 'is_active',]





admin.site.register(Photograph, PhotographAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(MontageSubFolder, MontageSubFolderAdmin)
admin.site.register(MontagePublicationFolder, MontagePublicationFolderAdmin)
