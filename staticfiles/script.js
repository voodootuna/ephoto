
$(document).ready(function() {
    var sendToMontage = [];
    $("#selectMontageBtn").click(function() {

         $("#selectMontageTxt").show();

        $(".image_href").prop("onclick", null)
        
        $("#selectMontageBtn").toggle();
        $("#toMontageBtn").toggle();
        $("#closeMontageBtn").toggle();

        $("img").each(function(div){
            $(this).addClass( "checkable" );
        });


        $("img.checkable").imgCheckbox({
        addToForm:false,
        onclick: function(el){
                var isChecked = el.hasClass("imgChked"),
                imgEl = el.children()[0];  // the img element
                var checkbox =  $(el).parent().parent().children('.montageCheckbox');
                checkbox.prop("checked", (isChecked? true:false));
                console.log(imgEl.name + " is now " + (isChecked? "checked": "not-checked") + "!");
                }
        });
      

    }); 

    $("#closeMontageBtn").click(function() {
        console.log('close montage');
        location.reload();
   
      });

    $('.datepicker').datepicker({
            uiLibrary: 'bootstrap4',
       
        });

     $('.datepicker2').datepicker({
            uiLibrary: 'bootstrap4',
       
        });

  $('.link-formset').formset({
        addText: '+',
        deleteText: '-',
        prefix: 'images',
        addCssClass: 'formset-btn',
        deleteCssClass:'formset-btn',
        formCssClass: 'form-inline',
    });


var uploadField = $('.form-control-file').change(function(){
        if(this.files[0].size > 3058789){
               alert("File is too big : "+(Math.round(this.files[0].size/1048576).toFixed(2))+'MB. Maximum file size allowed is 3MB');
               this.value = "";
            };

});







});





