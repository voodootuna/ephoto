
$(document).ready(function() {
    var sendToMontage = [];
    
    $("#selectMontageBtn").click(function() {
            
        $(".image_href").prop("onclick", null);
        $("#selectMontageTxt").show();

        $("#selectMontageBtn").toggle();
        $("#toMontageBtn").toggle();
        $("#closeMontageBtn").toggle();

        $("img").each(function(div){
            $(this).addClass( "checkable" );
        });


        $("img.checkable").imgCheckbox({
        addToForm:false,
        onclick: function(el){
                var isChecked = el.hasClass("imgChked"),
                imgEl = el.children()[0];  // the img element
                var checkbox =  $(el).parent().parent().children('.montageCheckbox');
                checkbox.prop("checked", (isChecked? true:false));
                console.log(imgEl.name + " is now " + (isChecked? "checked": "not-checked") + "!");
                }
        });
      
    }); 

    $("#closeMontageBtn").click(function() {
        console.log('close montage');
        location.reload();
   
      });

    $('.datepicker').datepicker({
            uiLibrary: 'bootstrap4',
       
        });

     $('.datepicker2').datepicker({
            uiLibrary: 'bootstrap4',
       
        });

  $('.link-formset').formset({
        addText: '+',
        deleteText: '-',
        prefix: 'images',
        addCssClass: 'formset-btn',
        deleteCssClass:'formset-btn',
        formCssClass: 'form-inline',
    });


var uploadField = $('.form-control-file').change(function(){
        if(this.files[0].size > 3058789){
               alert("File is too big : "+(Math.round(this.files[0].size/1048576).toFixed(2))+'MB. Maximum file size allowed is 3MB');
               this.value = "";
            };

});



// MONTAGE -----------

    var init_select = $('#publications').val()
    var selected_images




    $('#toMontageBtn').click(function(){
        selected_images  = []
        $('#exampleModal').modal({'show':true});
        $("#imageForm input[type='checkbox']").each(function(){
                var input = $(this); // This is the jquery object of the input, do what you will
                console.log( $(this).is(":checked"));
                if ($(this).is(":checked")) {
                    selected_images.push($(this).attr('name'));
                }
            });
            console.log(selected_images);
        $("#selected_images").html('')
        $("#selected_images").append('<p>'+selected_images+'</p>');

        //append selected images to form
        var images_input = $('<input>').attr("type", "hidden").attr("name", "images").val(selected_images);
        
        $('#montageForm').append(images_input);

    });


    $.each(num[init_select], function(key, value) {
            var txt = new Date(value).toDateString();
        $('#pub-dates').append($('<option></option>').attr('value', value).text(txt));

    });

    $('#dossiers').change(function(){
         if($(this).val()==='new') {
             $('#new-dossier-container').show();
                $('#new-dossier-input').prop('required', true)
            }else{
                $('#new-dossier-container').hide();
                $('#new-dossier-input').prop('required', false)
            }
    });


    $( "#publications" ).change(function() {
    
            var current_pub = $(this).val()
            var $elem = $('#pub-dates');
            var $elem2 = $('#dossiers');

    
            $elem.html('');

            $elem.trigger('change');
            $elem2.trigger('change');

            $elem.append($('<option selected disabled>---Choose publication date---</option>'));
            $.each(num[current_pub], function(key, value) {
                var txt = new Date(value).toDateString();
            $elem.append($('<option></option>').attr('value', value).text(txt));
            });

        });


    //toggle new dossier input element if select dossier = new
    $( "#pub-dates" ).change(function() {
            var $elem2 = $('#dossiers');
            $elem2.html('');

            $('#new-dossier-container').hide();
            $('#new-dossier-input').prop('required', false)

            $.getJSON('/montage_api/'+$(this).val()+'/'+$('#publications').val(), function() {
            })

            .done(function(data) { 
                 $elem2.html(' ');
              
                $.each(data, function(key, val) {
                                    console.log(val.pk, val.fields.title)
                    })  

                    $.each(data, function(key, val) {
                    $elem2.append($('<option></option>').attr('value', val.pk).text(val.fields.title));
                    });

                

                
            })
            .fail(function(data) { 
                console.log('get json error', data);

                $elem2.html('');
                
            
            }).always(function(){
                $elem2.append($('<option disabled selected value>-- select an option --</option>'));
                $elem2.append($('<option value="new">New dossier</option>'));

                
            });
        
        });

    $('#montageForm').submit(function(e){

       
        var values = {};
        
        var $inputs = $('#montageForm :input');
        $inputs.each(function() {
            values[this.name] = $(this).val();

        });
        console.log(values)
    })
    





});





