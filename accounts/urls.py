from django.urls import path
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from .forms import CustomAuthenticationForm
from django.contrib.auth import views as auth_views


urlpatterns = [

    url(r'signin/$', auth_views.LoginView.as_view(form_class=CustomAuthenticationForm), name='login'),
    url(r'signout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout')
]


