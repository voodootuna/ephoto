from django import forms
from django.contrib.auth.forms import AuthenticationForm

class CustomAuthenticationForm(AuthenticationForm):

	def __init__(self, *args, **kwargs):
		super(CustomAuthenticationForm, self).__init__(*args, **kwargs)
		for field_name, field in self.fields.items(): 
			print('FIELD', field_name)
			field.widget.attrs['class'] = 'form-control'