
from django.contrib.auth.mixins import LoginRequiredMixin

class CustomLoginRequiredMixin(LoginRequiredMixin):
	login_url = '/accounts/signin/'
	logout_url = '/accounts/signout/'